<?php
    // start the session
    session_start();
    require '../includes/Validate.php';
    require '../includes/QueryHandler.php';
    $db_handle = new QueryHandler;
    if (isset($_SESSION['username'])) {
        header('location:../index.php');
    } else {
        $userErr = $passwordErr = $conPasswordErr = '';
        if (isset($_POST['submit'])) {
            $valid = new Validate();
            $userErr = $valid->validateUser($_POST['username']);
            $passwordErr = $valid->validatePassword($_POST['password']);
            $conPasswordErr = $valid->matchValidation($_POST['password'], $_POST['con_password']);
            if (empty($userErr) && empty($passwordErr) && empty($conPasswordErr)) {
                $_SESSION['username'] = $_POST['username'];
                $hash = password_hash($_POST['password'], PASSWORD_DEFAULT);
                $sql = $db_handle->insert("INSERT INTO user (username, password, score) VALUES ('".$_POST['username']."', '$hash', '0')");
                header('Location:../index.php');
            }
        }
    }
?>
<!doctype html>
<!-- If multi-language site, reconsider usage of html lang declaration here. -->
<html lang="en"> 
    <head>
        <meta charset="utf-8">
        <title>Registration Form</title>
        <!-- View-port Basics: http://mzl.la/VYREaP -->
        <!-- This meta tag is used for mobile device to display the page without any zooming,
            so how much the device is able to fit on the screen is what's shown initially. 
            Remove comments from this tag, when you want to apply media queries to the website. -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <!-- Place favicon.ico in the root directory: mathiasbynens.be/notes/touch-icons -->
        <link rel="shortcut icon" href="favicon.ico" />
        <!--font-awesome link for icons-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <!-- Default style-sheet is for 'media' type screen (color computer display).  -->
        <link rel="stylesheet" media="screen" href="../assets/css/style.css" > 
    </head>
    <body>
        <!--container start-->
        <div class="container">
            <!--main section start-->
            <main>
                <section class="register-here">
                    <div class="wrapper">
                        <h1>Registration Form</h1>
                        <form class="sign-up" method="POST">
                            <label for="username">Username&ast;</label>
                            <div class="user">
                                <input type="text" name="username">
                                <span class="error"><?php echo $userErr;?></span>
                            </div>
                            <label for="password">Password&ast;</label>
                            <div class="pass">
                                <input type="password" name="password">
                                <span class="error"><?php echo $passwordErr;?></span>
                            </div>
                            <label for="con_password">Confirm Password&ast;</label>
                            <div class="confirm-pass">
                                <input type="password" name="con_password">
                                <span class="error"><?php echo $conPasswordErr;?></span>
                            </div>
                            <button class="register-detail" type="submit" name="submit" value="Upload">Register</button>
                        </form>
                        <a href="login.php" title="Login">Already registered? Login</a>
                    </div>
                </section>
            </main>
            <!--main section end-->
        </div>
    </body>
</html>