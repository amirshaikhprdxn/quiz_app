<?php
    session_start();
    include "../includes/QueryHandler.php";
    $db_handle = new QueryHandler;
    $conn = $db_handle->connect();
    //Check to see if score is set_error_handler
	if (!isset($_SESSION['score'])) {
		$_SESSION['score'] = 0;
	}
    //Check if form was submitted
    if ($_POST['submit']) {
    	$number = $_POST['number'];
    	$selected_choice = $_POST['choice'];
    	$next = $number+1;
    	$total = 4;
    	//Get total number of questions
    	$query = "SELECT * FROM `question`";
    	$results = $conn->query($query);
    	$total = $results->num_rows;
    	//Get correct choice
    	$q = "SELECT * FROM `choice` WHERE quest_no = $number AND is_correct=1";
    	$result = $conn->query($q);
    	$row = $result->fetch_assoc();
    	$correct_choice = $row['choice_id'];
    	//compare answer with result
    	if ($correct_choice == $selected_choice) {
    		$_SESSION['score']++;
    	}
        $updt = $conn->query("UPDATE user SET score = '".$_SESSION['score']."' WHERE username = '".$_SESSION['username']."'");
    	if ($number == $total) {
            $_SESSION['done'] = $number;
    		header("Location: final.php");
    	} else {
            header("Location: question.php?n=".$next."&score=".$_SESSION['score']);
    	}
    }
?>
