<?php
    session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Quiz App</title>
        <link rel="stylesheet" href="../assets/css/style.css" type="text/css" />
    </head>
    <body>
        <header>
            <div class="wrapper">
                <h1>Quiz App</h1>
            </div>
        </header>
        <main>
            <div class="wrapper">
                <p><strong>Congrats! You have completed the test</strong></p>
                <p><strong>Final score: <?php echo $_SESSION['score']; ?></strong></p>
                <a class="leader-board" href="leader_board.php" title="Leader Board">Leader Board</a>
            </div>
        </main>
        <footer>
            <div class="wrapper">
                Copyright &copy; 2020, Quiz App.
            </div>
        </footer>
    </body>
</html>