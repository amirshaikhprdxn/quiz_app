<?php
	session_start();
    include "../includes/QueryHandler.php";
    $db_handle = new QueryHandler;
	$conn = $db_handle->connect();
	$show_users = $conn->query("SELECT * FROM user ORDER BY score DESC");
?>
<!DOCTYPE html>
<html>
	<head>
	<meta charset="utf-8" />
		<title>Quiz App</title>
		<link rel="stylesheet" href="../assets/css/style.css" type="text/css" />
	</head>
	<body>
		<div id="container">
			<header>
			    <div class="wrapper">
			    	<h1>Quiz App</h1>
                	<a href="../index.php" class="back">Go Back</a>
                    <a class="logout" href="logout.php" title="Logout">Logout</a>
				</div>
			</header>
			<main>
				<div class="wrapper">
					<h2>Rankings</h2>
					<ul>
						<li class="rankings">
						  	<span>Usename</span>
						  	<span>Score</span>
						</li>
					    <?php
					    while ($row = $show_users->fetch_assoc()):
						?>
						<li class="rankings">
						  	<span><?php echo $row['username']; ?></span>
						  	<span><?php echo $row['score']; ?></span>
						</li>
						<?php endwhile; ?>
					</ul>
				</div>
			</main>
			<footer>
				<div class="wrapper">
					Copyright &copy; 2020, Quiz App.
				</div>
			</footer>
		</div>
	</body>
</html>