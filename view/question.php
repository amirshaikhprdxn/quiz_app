<?php
	session_start();
    include "../includes/QueryHandler.php";
    $db_handle = new QueryHandler;
	//Set question number
	$number = (int) $_GET['n'];
	$conn = $db_handle->connect();
	//Get total number of questions
	$results = $conn->query("SELECT * FROM question");
	$total = $results->num_rows;
	// Get questions
	$result = $conn->query("SELECT * FROM `question` WHERE quest_no = $number");
	$question = $result->fetch_assoc();
	// Get Choices
  	$choices = $conn->query("SELECT * FROM `choice` WHERE quest_no = $number");
?>

<!DOCTYPE html>
<html>
	<head>
	<meta charset="utf-8" />
		<title>Quiz App</title>
		<link rel="stylesheet" href="../assets/css/style.css" type="text/css" />
	</head>
	<body>
		<div id="container">
			<header>
			    <div class="wrapper">
                    <?php
                    	if (!isset($_SESSION['username'])) {
                        	header('Location:login.php');
                    ?>
			    	<h1>Quiz App</h1>
				</div>
			</header>
			<main>
				<div class="wrapper">
                    <?php
                        } else {
                	?>
					<div class="current">Question <?php echo $number; ?> of <?php echo $total; ?></div>
					<p class="question">
					<?php echo $question['questions'] ?>
					</p>
					<form method="post" action="process.php">
						<ul class="choices">
						    <?php
						    while ($row = $choices->fetch_assoc()):
					    	?>
						<li>
							<input name="choice" type="radio" value="<?php echo $row['choice_id']; ?>" />
						  	<?php echo $row['options']; ?>
						</li>
							<?php endwhile; ?>
						</ul>
						<input type="submit" value="submit" name="submit" />
						<input type="hidden" name="number" value="<?php echo $number; ?>" />
					</form>
                <?php
                    }
                ?>
				</div>
			</main>
			<footer>
				<div class="wrapper">
					Copyright &copy; 2020, Quiz App.
				</div>
			</footer>
		</div>
	</body>
</html>