<?php
    session_start();
    include "includes/QueryHandler.php";
    $db_handle = new QueryHandler;
?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Quiz App</title>
        <meta name="description" lang="en" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="shortcut icon" href="favicon.ico" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" media="screen" href="assets/css/style.css" >
    </head>
    <body>
        <!--container start-->
        <div class="container">
            <!--header section start-->
            <header>
                <div class="wrapper">
                    <h1>Quiz App</h1>
                    <?php
                    if (!isset($_SESSION['username'])) {
                    ?>
                    <a class="register" href="view/register.php" title="Register">Register</a>
                    <a class="login" href="view/login.php" title="Login">Login</a>
                    <h2>Test your General Knowledge</h2>
                    <p>This is a multiple choice quiz to test your knowledge</p>
                    <a href="view/question.php?n=1" class="start">Start Quiz</a>
                </div>
            </header>
            <!--header section ends-->
            <!--main section start-->
            <main>
                <div class="quiz">
                    <div class="wrapper">
                        <?php
                            } else {
                                //Get Results
                                $results = $db_handle->runQuery("SELECT * FROM question");
                                $total = count($results);
                        ?>
                        <a class="logout" href="view/logout.php" title="Logout">Logout</a>
                        <a class="leader-board" href="view/leader_board.php" title="Leader Board">Leader Board</a>
                        <h2>Hii <strong><?php echo $_SESSION['username'];?></strong>, Test your General Knowledge</h2>
                        <p>This is a multiple choice quiz to test your knowledge</p>
                        <ul>
                            <li><strong>Number of Questions: </strong><?php echo $total; ?></li>
                            <li><strong>Type: </strong>Multiple Choice</li>
                            <li><strong>Estimatd Time: </strong><?php echo $total*0.5; ?> minutes</li>
                        </ul>
                        <?php
                        if (!isset($_SESSION['done'])) {
                        ?>
                        <a href="view/question.php?n=1" class="start">Start Quiz</a>
                        <?php
                                }
                            }
                        ?>
                    </div>
                </div>
            </main>
            <!--main section end-->
            <!--footer section start-->
            <footer>
                <div class="wrapper">
                    <span>
                        Copyright &copy; 2020, Quiz App.
                    </span>
                </div>
            </footer>
            <!--footer section end-->
        </div>
        <!--container end-->
    </body>
</html>