<?php
    include_once 'DbConn.php';
    
    class QueryHandler extends DbConn
    {
        public function runQuery($query)
        {
            $conn = $this->connect();
            $result = mysqli_query($conn, $query);
            while ($row = mysqli_fetch_assoc($result))
                $resultset[] = $row;
            if (!empty($resultset))
                return $resultset;
        }
        
        public function numRows($query)
        {
            $conn = $this->connect();
            $result  = mysqli_query($conn, $query);
            $rowcount = mysqli_num_rows($result);
            return $rowcount;
        }

        public function insert($query)
        {
            $conn = $this->connect();
            $ret = mysqli_query($conn, $query);
            return $ret;
        }

        public function login($username, $pass)
        {
            $conn = $this->connect();
            $query = "SELECT * FROM user WHERE username = '$username'";
            $result = mysqli_query($conn, $query);
            if (mysqli_num_rows($result)) {
                while ($row = mysqli_fetch_array($result)) {
                    if (password_verify($pass, $row['password'])) {
                        return true;
                    } else {
                        return false;
                    }
                }
            } else {
                return false;
            }
        }

        public function logout()
        {
            session_start();
            // clean the session variable
            session_unset();
            // destroy the session
            session_destroy();
            header('Location:../index.php');
        }
    }
?>
