<?php
    class DbConn
    {
        private $servername;
        private $username;
        private $password;
        private $db;
        public function connect() {
            try {
                $this->servername = "localhost";
                $this->username = "root";
                $this->password = "";
                $this->db = "quiz_app";
                // Create connection
                $conn = new mysqli($this->servername, $this->username, $this->password,$this->db);
                // Check connection
                if ($conn->connect_error)
                    die("Connection failed: " . mysqli_connect_error());
                return $conn;
            }
            catch(Exception $e) {
                echo "There is some problem in connection: " . $e->getMessage();
            }
        }
    }
?>
