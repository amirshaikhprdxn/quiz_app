<?php
    class Validate
    {
        // username user
        public function validateUser($username)
        {
            if (empty($username)) {
                return "Username is required";
            }
        }

        // password validation
        public function validatePassword($password)
        {
            if (empty($password)) {
                return "Password is required";
            }
        }

        // match password validation
        public function matchValidation($password, $cPassword)
        {
            if ($password != $cPassword) {
                return "Password Dosen't match";
            } elseif (empty($cPassword)) {
                return "Password is required";
            }
        }
    }
?>
