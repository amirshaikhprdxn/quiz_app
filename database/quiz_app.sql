-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 01, 2020 at 02:14 PM
-- Server version: 5.7.19
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `quiz_app`
--

-- --------------------------------------------------------

--
-- Table structure for table `choice`
--

DROP TABLE IF EXISTS `choice`;
CREATE TABLE IF NOT EXISTS `choice` (
  `choice_id` int(10) NOT NULL AUTO_INCREMENT,
  `quest_no` int(10) NOT NULL,
  `is_correct` tinyint(1) NOT NULL,
  `options` varchar(255) NOT NULL,
  PRIMARY KEY (`choice_id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `choice`
--

INSERT INTO `choice` (`choice_id`, `quest_no`, `is_correct`, `options`) VALUES
(1, 1, 0, 'Goa'),
(2, 1, 0, 'Maharashtra'),
(3, 1, 1, 'Delhi'),
(4, 1, 0, 'Kolkata'),
(5, 2, 0, 'Maharashtra'),
(6, 2, 1, 'Goa'),
(7, 2, 0, 'Rajhasthan'),
(8, 2, 0, 'Kolkata'),
(9, 3, 0, 'Dr. Manmohan Singh'),
(10, 3, 1, 'Narendra Modi'),
(11, 4, 1, 'Statue of Unity'),
(12, 4, 0, 'Statue of Equality'),
(13, 4, 0, 'Statue of liberty'),
(14, 5, 0, 'Yuvraj Singh'),
(15, 5, 0, 'M.S. Dhoni'),
(16, 5, 0, 'Virat Kohli'),
(17, 5, 1, 'Sachin Tendulkar');

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;
CREATE TABLE IF NOT EXISTS `question` (
  `quest_no` int(10) NOT NULL AUTO_INCREMENT,
  `questions` varchar(255) NOT NULL,
  PRIMARY KEY (`quest_no`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `question`
--

INSERT INTO `question` (`quest_no`, `questions`) VALUES
(1, 'Capital of India'),
(2, 'Smallest state of India'),
(3, 'PM of India'),
(4, 'Tallest statue in the World'),
(5, 'God of cricket in India');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(25) NOT NULL,
  `password` char(60) NOT NULL,
  `score` int(10) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `username`, `password`, `score`) VALUES
(6, 'amir', '$2y$10$4hH.d2fH6GnFU0YzggLmzeXKf/ExdopDOQcoE1.unKARYSYWhbLD.', 1),
(7, 'faraz', '$2y$10$b9dPMXtfmFEBV1GtARJBoe/yO7MmyV/eQlbU5.hAs4yesB3C9mBu.', 3),
(8, 'emir', '$2y$10$wwqAkaKpYQ8vGbxQhSmQI./zdHKLBPc89vcnHrf9LZ3HdQB8Z4qPW', 3),
(9, 'zakir', '$2y$10$8qN/ywbVDofw5y7pN7Xtr.lIBBvPoh2SBZXpKY1jVYD5IdxwgEzAW', 5);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
